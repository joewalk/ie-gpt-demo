# Discover Ireland

## Overview
This project is a simple website to showcase the beauty and charm of Ireland, featuring information about major regions, top destinations, and contact information. It includes dynamically loaded data for destinations and current weather conditions.

## Prerequisites
- Python 3.x
- Flask (for running the weather proxy)

## Setup Instructions

### Step 1: Clone the Repository
Clone the repository to your local machine.
```bash
git clone <repository-url>
cd <repository-directory>
```

### Step 2: Prepare the Environment
Make sure you have Python 3 and Flask installed. If not, you can install Flask using pip:
```bash
pip install flask
```

### Step 3: Run the Flask Proxy
Run the Flask proxy server to enable the weather API proxy:
```bash
python proxy_server.py
```

### Step 4: Start the HTTP Server
In another terminal, navigate to the project directory and start a simple HTTP server to serve the website:
```bash
# For Python 3.x
python -m http.server 8000
```

### Step 5: Access the Website
Open your web browser and navigate to `http://localhost:8000`. You should see the home page of the Discover Ireland website.

## Directory Structure
```
project-directory/
│
├── index.html
├── regions.html
├── destinations.html
├── destination.html
├── contact.html
├── styles.css
├── script.js
├── destinations.json
├── images.json
├── proxy_server.py
├── images/
│   ├── destinations/
│       ├── aran_islands.png
│       ├── belfast_city_hall.png
│       ├── birr_castle_demesne.png
│       ├── bishops_palace.png
│       ├── blarney_castle.png
│       ├── brú_na_bóinne.png
│       ├── bunratty_castle.png
│       ├── cahir_castle.png
│       ├── carrick-a-rede_rope_bridge.png
│       ├── charles_fort.png
│       ├── chester_beatty_library.png
│       ├── christ_church_cathedral.png
│       ├── cliffs_of_moher.png
│       ├── clonmacnoise.png
│       ├── connemara_national_park.png
│       ├── cork_city_gaol.png
│       ├── croke_park.png
│       ├── desmond_castle.png
│       ├── dingle_oceanworld_aquarium.png
│       ├── dingle_peninsula.png
│       ├── dublin_castle.png
│       ├── dublin_zoo.png
│       ├── dublinia.png
│       ├── dún_aonghasa.png
│       ├── fota_wildlife_park.png
│       ├── galway_city_museum.png
│       ├── giants_causeway.png
│       ├── glendalough.png
│       ├── glenveagh_national_park.png
│       ├── guinness_storehouse.png
│       ├── hook_lighthouse.png
│       ├── inishowen_peninsula.png
│       ├── killarney_national_park.png
│       ├── killary_fjord.png
│       ├── killary_harbour.png
│       ├── kilmainham_gaol.png
│       ├── king_johns_castle.png
│       ├── kinsale.png
│       ├── kylemore_abbey.png
│       ├── lough_gur.png
│       ├── malin_head.png
│       ├── marble_arch_caves.png
│       ├── mizen_head.png
│       ├── mount_stewart.png
│       ├── muckross_abbey.png
│       ├── muckross_house.png
│       ├── national_botanic_gardens.png
│       ├── national_gallery_of_ireland.png
│       ├── newgrange.png
│       ├── phoenix_park.png
│       ├── poulnabrone_dolmen.png
│       ├── powerscourt_estate.png
│       ├── ring_of_kerry.png
│       ├── rock_of_cashel.png
│       ├── rock_of_dunamase.png
│       ├── ross_castle.png
│       ├── skellig_michael.png
│       ├── slieve_league.png
│       ├── spike_island.png
│       ├── st_michans_church.png
│       ├── st_patricks_cathedral.png
│       ├── the_ark_open_farm.png
│       ├── the_book_of_kells_and_the_old_library_exhibition.png
│       ├── the_burren.png
│       ├── the_custom_house.png
│       ├── the_english_market.png
│       ├── the_little_museum_of_dublin.png
│       ├── the_national_museum_of_ireland.png
│       ├── trinity_college_dublin.png
│       ├── wild_atlantic_way.png
│   ├── regions/
│       ├── clare.jpg
│       ├── cork.jpg
│       ├── donegal.jpg
│       ├── dublin.jpg
│       ├── galway.jpg
│       ├── kerry.jpg
│       ├── kilkenny.jpg
│       ├── limerick.jpg
│       ├── mayo.jpg
│       ├── sligo.jpg
│       ├── waterford.jpg
│       ├── wicklow.jpg
│   ├── scenic/
│       ├── scenic1.png
│       ├── scenic2.png
│       ├── scenic3.png