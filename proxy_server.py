from flask import Flask, request, jsonify
import requests
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/weather_proxy')
def weather_proxy():
    lat = request.args.get('lat')
    long = request.args.get('long')
    if not lat or not long:
        return jsonify({'error': 'Missing latitude or longitude'}), 400

    url = f"http://metwdb-openaccess.ichec.ie/metno-wdb2ts/locationforecast?lat={lat};long={long}"
    
    response = requests.get(url)
    if response.status_code != 200:
        return jsonify({'error': 'Failed to fetch data'}), response.status_code

    return response.content, response.status_code, {'Content-Type': 'application/xml'}

if __name__ == '__main__':
    app.run(port=5000)
