// script.js

document.addEventListener('DOMContentLoaded', () => {
    if (document.getElementById('destinations-list')) {
      fetch('destinations.json')
        .then(response => response.json())
        .then(data => {
          displayDestinations(data);
        })
        .catch(error => console.error('Error fetching data:', error));
    }
  
    const costFilter = document.getElementById('cost-filter');
    if (costFilter) {
      costFilter.addEventListener('change', () => {
        const filterValue = costFilter.value;
        fetch('destinations.json')
          .then(response => response.json())
          .then(data => {
            displayDestinations(data, filterValue);
          })
          .catch(error => console.error('Error fetching data:', error));
      });
    }
  
    const contactForm = document.getElementById('contactForm');
    if (contactForm) {
      contactForm.addEventListener('submit', function (event) {
        event.preventDefault();
  
        const formData = new FormData(contactForm);
        const name = formData.get('name');
        const email = formData.get('email');
        const phone = formData.get('phone');
        const comments = formData.get('comments');
  
        const data = {
          name,
          email,
          phone,
          comments
        };
  
        // Log the data to the console
        console.log('Form Data:', data);
  
        // Alert the user that the data is being logged to the console
        alert('Thank you for contacting us, ' + name + '! Your information is being logged to the console.');
  
        // Reset the form after submission
        contactForm.reset();
      });
    }
  });
  
  function displayDestinations(destinations, filter = 'all') {
    const destinationsList = document.getElementById('destinations-list');
    destinationsList.innerHTML = '';
  
    destinations
      .filter(destination => {
        if (filter === 'low') return destination.cost < 10;
        if (filter === 'medium') return destination.cost >= 10 && destination.cost < 20;
        if (filter === 'high') return destination.cost >= 20;
        return true;
      })
      .forEach(destination => {
        const listItem = document.createElement('li');
        listItem.innerHTML = `
          <h3><a href="destination.html?name=${encodeURIComponent(destination.name)}">${destination.name}</a></h3>
          <p><strong>Address:</strong> ${destination.address}</p>
          <p><strong>Cost:</strong> €${destination.cost}</p>
          <p><strong>Rating:</strong> ${destination.star_rating} stars</p>
        `;
        destinationsList.appendChild(listItem);
      });
  }
  